/**
 * com.hotels.model contains the model objects relating to the SOAP service. CXF takes care of the SOAP to POJO conversion.
 */
package com.hotels.model;