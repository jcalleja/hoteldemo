package com.hotels.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hotels.raml.api.model.ApiBooking;

public class Store {

	private Map<Integer, ApiBooking> db;
//	
//	private Integer count = 1;
//	
//	public synchronized void incrementCount() {
//		count++;
//	}
//	
	public Store() {
		db = new HashMap<Integer, ApiBooking>();
//		ApiBooking booking = new Booking();
//		booking.setArrivalDate(new Date());
//		booking.setDepartureDate(new Date());
//		booking.setRoomName("A20");
//		booking.setId(count);
//		db.put(count, booking);
	}
	
	public Integer persistBooking(ApiBooking booking) {
		Integer id = booking.getId();
		db.put(id, booking);
		return id;
	}
	
	public List <ApiBooking> getAllBookings() {
		System.out.println("db: " + db);
		
		return new ArrayList<ApiBooking>(db.values());
	}
	
}
