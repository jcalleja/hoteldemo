package com.hotels.api;

import java.util.List;

import javax.jws.WebService;

import com.hotels.model.Booking;
import com.hotels.model.CreatedEntity;

@WebService
public interface BookingService {
	
	public List<Booking> getBookings();

	public CreatedEntity persistBooking(Booking booking);
}
