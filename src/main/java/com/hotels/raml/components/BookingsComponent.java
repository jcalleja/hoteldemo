package com.hotels.raml.components;

import java.util.List;

import com.hotels.raml.api.model.ApiBooking;
import com.hotels.raml.api.model.ApiBookings;
import com.hotels.raml.api.model.ApiCreatedEntity;
import com.hotels.store.Store;

public class BookingsComponent {
	
	public Store store;
	
	public void setStore(Store db) {
		this.store = db;
	}
	
	public Store getStore() {
		return store;
	}
	
	public BookingsComponent() {
	}
	
	public ApiCreatedEntity persistBooking(ApiBooking booking) {
		Integer id = store.persistBooking(booking);
		ApiCreatedEntity response = new ApiCreatedEntity();
		response.setId("" + id);
		return response;
	}

	public ApiBookings getBookings(String arg) {
		return getBookings();
	}
	
	public ApiBookings getBookings() {
		List<ApiBooking> bookingsList = store.getAllBookings();
		ApiBookings apiBookings = new ApiBookings();
		apiBookings.setBookings(bookingsList);
		apiBookings.setSize(bookingsList.size());
		return apiBookings;
	}
}
