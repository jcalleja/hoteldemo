/**
 * com.hotels.raml.api.model contains the model objects relating to the REST service. They use Jackson annotations to be able to be converted to JSON.
 */
package com.hotels.raml.api.model;